(function ($) {
  'use strict';

  $.fn.exists = function () {
    return this.length !== 0;
  };

  $(document).ready(function () {
    var validUrl = new RegExp('.*stackoverflow.com/questions/\\d+');
    var currentUrl = $(location).attr('href');
    // Storing only questions with accepted answers
    if (validUrl.test(currentUrl) && $('div#answers .answer.accepted-answer').exists()) {
      chrome.extension.sendMessage({
        action: 'addQuestion',
        data: getData(currentUrl)
      });
    }
  });

  // TODO need to sanitize the question and answer html
  var getData = function (url) {
    var questionId = $('div#question').data('questionid');
    var questionTitle = $('div#question-header a.question-hyperlink').text().trim();
    var question = $('div#question .post-text').html().trim();
    var acceptedAnswer = $('div#answers .answer.accepted-answer .post-text').html().trim();
    var tags = [];
    $('div#question .post-taglist .post-tag').each(function () {
      tags.push($(this).text().trim());
    });

    return {
      url: url,
      visitDate: Date.now(),
      question: question,
      questionId: questionId,
      questionTitle: questionTitle,
      answer: acceptedAnswer,
      tags: tags
    };
  };

})(jQuery);
