'use strict';

(function () {

  chrome.runtime.onInstalled.addListener(function (details) {
    console.log('previousVersion', details.previousVersion);
  });

  var storage = indexedDBStorageHelper();

  chrome.extension.onMessage.addListener(function (request, sender, sendResponse) {
    // TODO check data validity
    if (request && request.action === 'addQuestion') {
      var question = request.data;

      console.log('A stackoverflow entry received: ');
      console.log(question);

      chrome.browserAction.setBadgeText({text: '+', tabId: sender.tab.id});

      storage
        .findById(question.questionId)
        .then(function (item) {
          if (item === undefined) {
            storage.add(question);
          } else {
            console.log('repeated visit ... ignored');
          }
        });

    }
  });

})();