'use strict';

console.log('\'Allo \'Allo! Popup');

document.getElementById('list-button').addEventListener('click', function () {
  var appURl = chrome.extension.getURL('overflowise.html');
  // query and open or create
  chrome.tabs.query({url: appURl}, function (tabs) {
    if (tabs.length > 0) {
      chrome.tabs.update(tabs[0].id, {active: true});
    } else {
      chrome.tabs.create({
        url: appURl,
        active: true
      });
    }
  });
});
