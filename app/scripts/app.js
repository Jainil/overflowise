var app = angular.module('wiseApp', ['ui.router']);

app.service('DataService', indexedDBStorageHelper);

app.config(function ($stateProvider, $urlRouterProvider, $compileProvider) {

  // http://stackoverflow.com/questions/15606751/angular-changes-urls-to-unsafe-in-extension-page
  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);

  $urlRouterProvider.otherwise('/questions');

  $stateProvider
    .state('questions', {
      url: '/questions',
      templateUrl: 'partials/questions.tpl.html',
      controller: 'QuestionListCtrl',
      resolve: {
        questions: ['DataService', function (DataService) {
          return DataService.list();
        }]
      }
    })
    .state('questionsByTag', {
      url: '/questions/:tagName',
      templateUrl: 'partials/questions.tpl.html',
      controller: 'QuestionListCtrl',
      resolve: {
        questions: ['DataService', '$stateParams', function (DataService, $stateParams) {
          return DataService.list({tag: $stateParams.tagName});
        }]
      }
    })
    .state('tags', {
      url: '/tags',
      templateUrl: 'partials/tags.tpl.html',
      controller: 'TagListCtrl',
      resolve: {
        tags: ['DataService', function (DataService) {
          return DataService.listTags();
        }]
      }
    })
    .state('overview', {
      url: '/overview',
      templateUrl: 'partials/overview.tpl.html'
    })
    .state('settings', {
      url: '/settings',
      templateUrl: 'partials/settings.tpl.html',
      controller: 'SettingsCtrl'
    });
});

app.run(['$rootScope', '$state', function ($rootScope, $state) {
  $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
    event.preventDefault();
    console.log(error);
  });
}]);

app.controller('QuestionListCtrl', ['$scope', 'questions', 'DataService', function ($scope, questions, DataService) {
  $scope.questions = questions;

  var loadMore = function () {
    DataService.list({page: 2}).then(function (newQuestions) {
      if (!newQuestions) {
        return;
      }
      $scope.questions.push(newQuestions[0]);
      $scope.$apply();
    }, function () {
      // do nothing if no more questions are found
    });
  };

  $(window).on('scroll', function () {
    if ($(window).scrollTop() >= $(document).height() - $(window).height()) {
      loadMore();
    }
  });
}]);

app.controller('TagListCtrl', ['$scope', 'tags', function ($scope, tags) {
  $scope.tags = tags;
}]);

app.controller('OverviewCtrl', ['$scope', function ($scope) {
  $scope.day = moment();
}]);

app.controller('SettingsCtrl', ['$scope', 'DataService', function ($scope, DataService) {
  var $fileinput = $('#fileinput');

  $scope.export = DataService.exportData;

  $scope.import = function () {
    if ($scope.f) {
      var r = new FileReader();

      r.onload = function (e) {
        var contents = e.target.result;
        try {
          var data = JSON.parse(contents);
          DataService.seedData(data).then(function () {
            $fileinput.val('');
            window.alert('data loaded successfully');
          });
        } catch (e) {
          // do nothing in ur life
        }
      };

      r.readAsText($scope.f);
    } else {
      window.alert('File not loaded');
    }
  };

  $fileinput.on('change', function (evt) {
    $scope.$apply(function (scope) {
      scope.f = evt.target.files[0];
    });
  });
}]);

app.filter('to_trusted', ['$sce', function ($sce) {
  return function (text) {
    return $sce.trustAsHtml(text);
  };
}]);