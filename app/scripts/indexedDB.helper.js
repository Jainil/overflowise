/* globals: Dexie, indexedDbStorageHelper */

/* deleting the whole database:

 var req = indexedDB.deleteDatabase('questions-database');
 req.onsuccess = function () {
 console.log("Deleted database successfully");
 };
 req.onerror = function () {
 console.log("Couldn't delete database");
 };
 req.onblocked = function () {
 console.log("Couldn't delete database due to the operation being blocked");
 };

 */

var indexedDBStorageHelper = function () {

  var Utils = {
    updateObject: function (orig, updt) {

      if (updt === undefined) {
        return orig;
      }

      for (var prop in orig) {
        if (orig.hasOwnProperty(prop)) {
          if (updt[prop] !== undefined) {
            orig[prop] = updt[prop];
          }
        }
      }

      return orig;
    }
  };


  var db = new Dexie('questions-database');
  db.version(1).stores({
    questions: 'questionId, questionTitle, *tags, visitDate',
    tags: '++id, tagName, count'
  });
  db.open();

  return {

    // list elements for a particular page, returns false if no more values are found
    list: function (config) {

      var promise = Promise.defer();

      var conf = {
        page: 0,
        elems: 1,
        tag: ''
      };

      Utils.updateObject(conf, config);

      var offset = conf.page * conf.elems;

      db.questions.count().then(function (count) {
        if (offset > count) {
          promise.reject();
        } else if (conf.tag !== '') {
          db.questions
            .where('tags').equals(conf.tag)
            .offset(offset).limit(conf.elems)
            .sortBy('visitDate')
            .then(promise.resolve);
        } else {
          return db.questions.orderBy('visitDate')
            .offset(offset).limit(conf.elems).toArray()
            .then(promise.resolve);
        }
      });

      return promise.promise;
    },

    listTags: function () {
      return db.tags.orderBy('count').desc().toArray();
    },

    addTags: function (tags) {
      return db.transaction('rw', db.questions, db.tags, function () {
        tags.forEach(function (tag) {
          db.tags
            .where('tagName')
            .equals(tag)
            .first(function (dbTag) {
              if (dbTag === undefined) {
                db.tags.add({tagName: tag, count: 1});
              } else {
                var count = dbTag.count ? dbTag.count + 1 : 1;
                db.tags.update(dbTag, {count: count});
              }
            });
        });
      });
    },

    add: function (question) {
      this.addTags(question.tags)
        .then(function () {
          db.tags.where('tagName').anyOf(question.tags).keys(function (tags) {
            question.tags = tags;
            db.questions.put(question);
          });
        })
        .catch(function (e) {
          console.log(e);
        });
    },

    // returns a promise that resolves with the question or undefined
    findById: function (questionId) {
      return db.questions.where('questionId').equals(questionId).first();
    },

    // returns a promise
    remove: function (question) {
      return db.questions.where('questionId').equals(question.questionId).delete();
    },

    // generates a json dump of db data
    exportData: function () {
      var promises = [];
      db.tables.forEach(function (table) {
        promises.push(db[table.name].toArray());
      });

      Promise.all(promises).then(function (data) {
        var dataToStore = {};

        for (var i = 0; i < data.length; i++) {
          dataToStore[db.tables[i].name] = data[i];
        }

        var serializedData = JSON.stringify(dataToStore);
        var link = $('#exportLink');
        link.attr('href', 'data:Application/octet-stream,' + encodeURIComponent(serializedData));
        link.attr('download', db.name + '.dump.json');
        link[0].click();
      });
    },

    // it is assumed that the data is to be imported into this db which is also empty
    seedData: function (data) {
      var p = Promise.defer();
      var promises = [];

      Object.keys(data).forEach(function (tableName) {
        data[tableName].forEach(function (doc) {
          promises.push(db[tableName].put(doc));
        });
      });

      Promise.all(promises).then(function () {
        p.resolve(true);
      });

      return p.promise;
    }

  };
};